#!/bin/bash

source install_util.sh

function print_help
{
    echo 'Usage: setup.sh [OPTION] [SOFTWARE]
    Options:
	-h --help         print this help
	-c --config       install config
	-u --update_deps  install or update installed files
	-f --fetch        -
        -i --install      install
    Artifacts/Software:
        dotfiles      Setup dotfiles
        deps          Install dependencies
	nodejs        NodeJS and Yarn
	bazel         Bazel build system (and emacs rust-mode)
	golang        Golang programming language (and emacs rust-mode)
	android       Android SDK
	rust(-mode)   Rust programming language (and emacs rust-mode)
	gcloud        Google Cloud SDK
	awscli        Amazon Web Services (AWS) SDK
	arangodb      ArangoDB database
	aerospike     Aerospike database
        pelikan       Twitter Pelican cache server
	redis         Redis server
        dart(-mode)          Dart SDK
	docker        Docker CE

Setup environment varialbles in ./.setup'
}

function install_deps
{
    sudo apt update && sudo apt full-upgrade -y
    sudo apt install -y \
	 apt-transport-https
	 git global build-essential \
	 autoconf emacs25-nox \
	 aria2 python-apt rlwrap texinfo \
	 openjdk-8-jdk cmake lldb llvm \
	 texinfo libsubunit-dev libpth-dev \
	 docker docker-compo
}

function setup_dotfiles
{
    cd $HOME
    echo 'cloning dotfiles...'
    clonerepo "git@gitlab.com:cmeo226/dotfiles" ~/dotfiles
    ln -sf --backup ~/dotfiles/.screenrc ~/.
    ln -sf --backup ~/dotfiles/.bash_profile ~/.
    ln -sf --backup ~/dotfiles/.bashrc ~/.
    ln -sf --backup ~/dotfiles/.bashrc_custom ~/.
    ln -sf --backup ~/dotfiles/.emacs.d ~/.
}

function config_git
{
    if [ ! -f ~/.ssh/id_rsa.pub ]; then
	ssh-keygen -C $EMAIL

	curl "{'key': $key, 'title': $HOSTNAME}" \
	     -H "Authorization: token $GITHUB_TOKEN" \
	     -H "X-OAuth-Scopes: repo, user" \
	     -H "X-Accepted-OAuth-Scopes: admin:public_key, user" \
	     -H "Accept: application/json" \
	     https://api.github.com/user/keys

	curl "{'key': key,'title': $HOSTNAME}" \
	     -H "PRIVATE-TOKEN: $GITLAB_TOKEN" \
	     https://gitlab.com/api/v3/user/keys
    fi

    git config --global user.name $NAME
    git config --global user.email $EMAIL

    setup_dotfiles

    git config --global alias.ignore \
	'!gi() { curl -L -s https://www.gitignore.io/api/$@ ;}; gi'

    if ! grep -q "# gitignore" ~/.bashrc; then
	echo '
# gitignore
function gi() { curl -L -s https://www.gitignore.io/api/$@ ;}
' >> ~/.bashrc
    fi
}

case "$1" in
    "-h"|"--help")
	print_help
	;;
    "-c"|"--config")
	shift
	case "$1" in
	    "deps")
		install_deps
		;;
	    "dotfiles")
		config_git
		;;
	    "")
		install_deps
		config_git
		;;
	    *)
		print_help
		;;
	esac
	;;
    "-u"|"--update_deps")
	install_deps
	;;
    "-f"|"--fetch")
	fetch
	;;
    "-i"|"--install")
	shift
	case "$1" in "") fetch; exit ;; esac
	for args in "$@"; do
	    case "$args" in
		"nodejs")
		    install_nodejs
		    ;;
		"bazel")
		    install_bazel
		    ;;
		"golang")
		    install_golang
		    ;;
		"android")
		    install_android_sdk
		    ;;
		"rust")
		    shift
		    case "$1" in
			"--mode")
			    install_rust_mode
			    ;;
			*)
			    install_rust
			    ;;
		    esac
		    exit
		    ;;
		"gcloud")
		    install_gcloud
		    ;;
		"awscli")
		    install_aws_sdk
		    ;;
		"arangodb")
		    install_arangodb
		    ;;
		"aerospike")
		    install_aerospike
		    ;;
		"pelikan")
		    install_check
		    install_pelikan
		    ;;
		"redis")
		    install_redis
		    ;;
		"dart")
		    shift
		    case "$1" in
			"--mode")
			    install_dart_mode
			    ;;
			*)
			    install_dart
			    ;;
		    esac
		    exit
		    ;;
		"docker")
		    install_docker
		    ;;
		*)
		    print_help
		    ;;
	    esac
	done
	;;
    *)
	echo "ERROR: Wrong arguments."
	print_help
	exit 3
	;;
esac
