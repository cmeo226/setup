#!/bin/bash

source ../install_util.sh

function mattermost
{
    ../setup.sh -f

    # install dependencies
    install_docker
    install_golang
    install_nodejs

    # Edit /etc/hosts to add dockerhost
    sudo echo "127.0.0.1" > /etc/hosts

    # add username to docker gfroup to run docker without sudo
    sudo gpassword -a $USER docker

    # restart docker daemon
    sudo service docker restart

    # change current group ID to docker group
    newgrp docker


    # install build-essential package
    sudo apt install build-essential

    # clone repo
    git clone git@github.com:cmeon/platform \
	GOPATH/src/github.com/mattermost/platform
}
