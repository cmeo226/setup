TMP=~/.tmp
mkdir -p $TMP
source ./.setup

function fetch
{
    envsubst < ./pkgs > $TMP/pkgs.v
    aria2c -i $TMP/pkgs.v -d $TMP
}

function install_nodejs
{
    if ! [ -f $TMP/node-${NODE_VERSION}-linux-x64.tar.xz ]; then
	fetch
    fi
    echo 'installing node.js...'
    cd $TMP
    tar xf node-${NODE_VERSION}-linux-x64.tar.xz
    rm -rf ~/node
    mv ./node-${NODE_VERSION}-linux-x64 ~/node
    cd

    if ! grep -q "# node" ~/.bashrc; then
	echo '
# node
export PATH="$PATH:$HOME/node/bin"
' >> ~/.bashrc
    fi

    # Yarn
    echo 'installing yarn...'
    rm -rf ~/.yarn
    curl -o- -L https://yarnpkg.com/install.sh |\
	bash -s -- --version ${YARN_VERSION}

    if ! grep -q 'export PATH="$HOME/.yarn/bin:$PATH"' ~/.bashrc; then
	echo '
export PATH="$HOME/.yarn/bin:$PATH"
' >> ~/.bashrc
    fi

    source ~/.bashrc

    # Typescript
    echo 'installing typescript...'
    yarn global add typescript
}

function install_typescript_mode
{
    if ! grep -q "typescript-mode" ~/.bashrc; then
    echo "
;; ---------------------
;; -- typescript-mode --
;; ---------------------
(add-hook 'typescript-mode-hook
	  (lambda ()
	    (tide-setup)
0	    (flycheck-mode +1)
	    (setq flycheck-check-syntax-automatically '(save mode-enabled))
	    (eldoc-mode +1)))
	    ;; company is an optional dependency. You have to
	    ;; install it separately via package-install
	    ;; (company-mode-on)))

;; aligns annotation to the right hand side
(setq company-tooltip-align-annotations t)

;; formats the buffer before saving
(add-hook 'before-save-hook 'tide-format-before-save)

;; format options
(setq tide-format-options
      '(:insertSpaceAfterFunctionKeywordForAnonymousFunctions
	t
	:placeOpenBraceOnNewLineForFunctions
	nil))
;; see https://github.com/Microsoft/TypeScript/blob/cc58e2d7eb144f0b2ff89e6a6685fb4deaa24fde/src/server/protocol.d.ts#L421-473 for the full list available options


;; format options
(setq tide-format-options
      '(:insertSpaceAfterFunctionKeywordForAnonymousFunctions
	t
	:placeOpenBraceOnNewLineForFunctions
	nil))
;; see https://github.com/Microsoft/TypeScript/blob/cc58e2d7eb144f0b2ff89e6a6685fb4deaa24fde/src/server/protocol.d.ts#L421-473 for the full list available options

;; Tide can be used along with web-mode to edit tsx files
;; (require 'web-mode)
(add-to-list 'auto-mode-alist '(\"\\.tsx\\'\" . web-mode))
(add-to-list 'auto-mode-alist '(\"\\.jsx\\'\" . web-mode))
(add-hook 'web-mode-hook
	  (lambda ()
	    (when (string-equal \"tsx\" (file-name-extension buffer-file-name))
	      (tide-setup)
	      (flycheck-mode +1)
	      (setq flycheck-check-syntax-automatically '(save mode-enabled))
	      (eldoc-mode +1))))
" >> ~/.emacs.d/init.el
    fi
}

function install_android_sdk
{
    if ! [ -f $TMP/sdk-tools-linux-${ANDROID_SDK_VERSION}.zip ]; then
	fetch
    fi
    echo 'installing android sdk...'
    mkdir -p ~/android/sdk/
    cd ~/android/sdk/ && unzip -q $TMP/sdk-tools-linux-${ANDROID_SDK_VERSION}.zip
    if ! grep -q "# android home"  ~/.bashrc; then
	echo '
# android home
export ANDROID_HOME=~/android/sdk/tools
export PATH=$PATH:$ANDROID_HOME:$ANDROID_HOME/bin:$ANDROID_HOME/bin64
export PATH=$PATH:$HOME/android/sdk/platform-tools
' >> ~/.bashrc
    fi
}

function install_golang
{
    if ! [ -f $TMP/go${GO_VERSION}.linux-amd64.tar.gz ]; then
	fetch
    fi
    echo 'installing golang...'
    rm -rf ~/go
    tar xzf $TMP/go${GO_VERSION}.linux-amd64.tar.gz -C ~/

    if ! grep -q "# golang" ~/.bashrc; then
	echo '
# golang
export GOROOT=$HOME/go
export PATH=$PATH:$GOROOT/bin
export GOPATH=$HOME
export PATH=$PATH:$GOPATH/bin
' >> ~/.bashrc
    fi

    # restart bash
    source ~/.bashrc

    # add go_tools
    go get -u -v golang.org/x/tools/...

    # gocode
    go get -u -v github.com/nsf/gocode
}

function install_go_mode
{
    # add go-mode for emacs
    if ! grep -q "go-mode.el" ~/.emacs.d/init.el; then
	echo "
;; ----------------
;; -- go-mode.el --
;; ----------------
(add-to-list 'load-path \"/place/where/you/put/it/\")
(require 'go-mode-autoloads)

(with-eval-after-load 'go-mode
  (require 'go-autocomplete)
  (require 'go-oracle)
  (setq go-oracle-scope "."))

(require 'auto-complete-config)
(ac-config-default)

(defun cmeon-go-mode-hook ()
  ; Use goimports instead of go-fmt
  (setq gofmt-command "goimports")
  ; Call Gofmt before saving
  (add-hook 'before-save-hook 'gofmt-before-save)
  ; auto-complete
  (auto-complete-mode 1)
  ; Customize compile command to run go build
  (if (not (string-match "go" compile-command))
      (set (make-local-variable 'compile-command)
	   "go generate && go build -v && go test -v && go vet"))
  ; Godef jump key binding
  (local-set-key (kbd "M-.") 'godef-jump))

(add-hook 'go-mode-hook 'cmeon-go-mode-hook)
" >> ~/.emacs.d/init.el
    fi
}

function install_rust
{
    echo 'installing rust...'
    curl https://sh.rustup.rs -sSf | sh

    if ! grep -q "# cargo and rust" ~/.bashrc; then
	echo '
# cargo and rust
source $HOME/.cargo/env
' >> ~/.bashrc
    fi
    install_rust_mode
}

function install_rust_mode
{
    clonerepo git@github.com:rust-lang/rust-mode ~/.emacs.d/lisp/rust-mode
    # add go-mode for emacs
    if ! grep -q "rust-mode.el" ~/.emacs.d/init.el; then
	echo "
;; ------------------
;; -- rust-mode.el --
;; ------------------
(add-to-list 'load-path \"~/.emacs.d/lisp/rust-mode/\")
(autoload 'rust-mode \"rust-mode\" nil t)
(add-to-list 'auto-mode-alist '(\"\\\\.rs\\\\'\" . rust-mode))
" >> ~/.emacs.d/init.el
    fi
}

function install_aws_sdk
{
    # AWS sdk
    echo 'installing aws shell...'
    sudo easy_install aws-shell
}

function install_gcloud
{
    # Gcloud
    # https://cloud.google.com/sdk/downloads?authuser=1#interactive
    echo 'installing gcloud...'
    mkdir -p "${HOME}/tools"
    curl https://sdk.cloud.google.com | bash -s -- --install-dir ${HOME}/tools
    source ~/.bashrc
    gcloud init
}

function install_bazel
{
    echo 'installing bazel...'
    echo "
deb [arch=amd64] http://storage.googleapis.com/bazel-apt stable jdk1.8" |\
	sudo tee /etc/apt/sources.list.d/bazel.list
    curl https://bazel.build/bazel-release.pub.gpg | sudo apt-key add -
    sudo apt-get update -q && sudo apt-get install bazel -q
}

function install_bazel_mode
{
    # add go-mode for emacs
    if ! grep -q "bazel-mode.el" ~/.emacs.d/init.el; then
	echo "
;; -------------------
;; -- bazel-mode.el --
;; -------------------
(add-to-list 'auto-mode-alist '(\"BUILD\" . python-mode))
(add-to-list 'auto-mode-alist '(\"WORKSPACE\"  . python-mode))
" >> ~/.emacs.d/init.el
    fi
}

function install_arangodb
{
    echo "installing arangodb..."
    wget https://www.arangodb.com/repositories/arangodb31/xUbuntu_16.04/Release.key
    apt-key add - < Release.key
    echo 'deb https://www.arangodb.com/repositories/arangodb31/xUbuntu_16.04/ /' | sudo tee /etc/apt/sources.list.d/arangodb.list
    sudo apt-get install apt-transport-https
    sudo apt-get update
    sudo apt-get install arangodb3=${ARANGODB_VERSION}
}

function install_redis
{
    if ! [ -f $TMP/redis-${REDIS_VERSION}.tar.gz ]; then
	fetch
    fi
    echo 'installing redis...'
    cd $TMP
    tar xzf redis-${REDIS_VERSION}.tar.gz
    cd redis-${REDIS_VERSION}
    make && sudo make install
}

function install_check
{
    clonerepo "https://github.com/libcheck/check"  $TMP/check
    echo 'installing check...'
    cd $TMP/check
    git checkout ${CHECK_COMMIT} .
    autoreconf --install
    make && make check && sudo make install
}

function install_pelikan
{
    clonerepo "https://github.com/twitter/pelikan" $TMP/pelikan
    echo 'installing pelikan...'
    cd $TMP/pelikan
    git checkout ${PELIKAN_COMMIT} .
    mkdir _build && cd _build
    cmake ..
    make -j && sudo make install
}

function clonerepo
{
    REPOSRC=$1
    LOCALREPO=$2
    LOCALREPO_VC_DIR=$LOCALREPO/.git
    if [ ! -d $LOCALREPO_VC_DIR ]
    then
	git clone $REPOSRC $LOCALREPO
    else
	cd $LOCALREPO
	git checkout master
	git pull $REPOSRC
    fi
}

function install_dart
{
    sudo apt-get update
    sudo apt-get install apt-transport-https
    sudo sh -c 'curl https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -'
    sudo sh -c 'curl https://storage.googleapis.com/download.dartlang.org/linux/debian/dart_stable.list > /etc/apt/sources.list.d/dart_stable.list'
    sudo apt-get update
    sudo apt-get install dart

    # aria2c https://storage.googleapis.com/dart-archive/channels/stable/release/latest/dartium/dartium-linux-x64-release.zip
    for x in $(ls /usr/lib/dart/bin/); do
	sudo ln -sf /usr/lib/dart/bin/$x /usr/bin/
    done
}

function install_dart_mode
{
    clonerepo git@github.com:nex3/dart-mode ~/.emacs.d/lisp/dart-mode
    # add go-mode for emacs
    if ! grep -q "dart-mode.el" ~/.emacs.d/init.el; then
	echo "
;; ------------------
;; -- dart-mode.el --
;; ------------------
(add-to-list 'load-path \"~/.emacs.d/lisp/dart-mode/\")
(autoload 'dart-mode \"dart-mode\" nil t)
(add-to-list 'auto-mode-alist '(\"\\\\.dart\\\\'\" . dart-mode))
(setq dart-enable-analysis-server t)
(add-hook 'dart-mode-hook 'flycheck-mode)
(add-hook 'before-save-hook 'dartfmt-before-save)
" >> ~/.emacs.d/init.el
    fi
}

function install_docker
{
    sudo apt remove docker docker.io
    sudo apt autoremove
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo apt-key fingerprint 0EBFCD88
    sudo add-apt-repository \
	 "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
              $(lsb_release -cs) \
              stable"
    sudo apt update
    sudo apt install docker-ce
    sudo gpasswd -a $USER docker && newgrp docker
}
